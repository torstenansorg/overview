'use strict';

/* global route,path */

(function () {

    var DB;
    var icons = {
        android: null,
        ios: null
    }

    function setStatusMessage(el, msg) {

    }


    function validateInput(input) {
        var val = input.val().toLowerCase();
        if((input.attr('id') === 'cms-domain' || input.attr('id') === 'web-domain') && val !== '') {
            if(val.match(/^https?:\/\//) === null) {
                input.val('https://'+val);
                val = 'https://'+val;
            }
        }
        if(input.attr('required') && val === '') {
            input.addClass('invalid');
        } else if(input.attr('filter')) {
            switch (input.attr('filter')) {
                case 'identifier':
                    if(val.match(/^([a-zA-Z0-9]*\.)*([a-zA-Z0-9]+)$/i) !== null && typeof DB.identifier !== 'undefined' && $.inArray(val, DB.identifier) === -1) {
                        input.removeClass('invalid');
                    } else {
                        input.addClass('invalid');
                    }
                    break;
                case 'domain':
                    if(val.match(/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/i) !== null) {
                        input.removeClass('invalid');
                    } else {
                        input.addClass('invalid');
                    }
                    break;
                default:
                    break;
            }
        } else {
            if(input.attr('id') === 'project-name') {
                if(typeof DB.folder !== 'undefined') {
                    var f = false;
                    for (var i = 0; i < DB.folder.length && !f; i++) {
                        if(DB.folder[i].toLowerCase() === val) {
                            f = true;
                        }
                    }
                    if(f) {
                        input.addClass('invalid');
                    } else {
                        input.removeClass('invalid');
                    }
                }
            } else {
                input.removeClass('invalid');
            }
        }
    }

    function handleFileUpload(preview, file) {
        preview = $(preview);
        var t = ($(preview).attr('id').replace('-icon', ''));
        icons[t] = file;
        if(file.type === 'image/png' && file.size > 0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var img = $('<img>', {
                    src: e.target.result
                });
                preview.html(img).removeClass('empty');
            }
            reader.readAsDataURL(file);
        }
    };

    function initMenuActions() {
        $('#save-button').on({
            click: function (e) {
                e.stopPropagation();
                $('input[required]').each(function () {
                    validateInput($(this));
                });

                if(!$('#identifier').hasClass('invalid')) {
                    $.get(route+path+'/tl', function (response) {
                        if($.inArray($('#identifier').val(), response.identifier) !== -1) {
                            $('#identifier').addClass('invalid');
                            setStatusMessage($('#identifier'), 'identifier already used');
                            return false;
                        } else {
                            if($.inArray($('#project-name').val(), response.folder) !== -1) {
                                $('#project-name').addClass('invalid');
                                setStatusMessage($('#project-name'), 'folder-name already used');
                                return false;
                            } else {
                                if(!$('#project-name').hasClass('invalid')) {
                                    var pic = (icons.ios !== null ? icons.ios : icons.android);

                                    if(pic !== null) {
                                        var icon = new FormData();
                                        icon.append('icon', pic);
                                    }

                                    var data = {
                                        name: $('#project-name').val(),
                                        description: $('#description').val(),  
                                        status: 'new',
                                        maturity: $('#maturity-input').val(),
                                        icon: null
                                    };


                                    $.ajax({
                                        url: route+path+'/save-icon',
                                        type: 'POST',
                                        data: icon,
                                        cache: false,
                                        dataType: 'json',
                                        processData: false,
                                        contentType: false,
                                        success: function(response, textStatus, jqXHR)
                                        {
                                            if(response.status == 'success' && typeof response.img !== 'undefined' && !response.img !== '') {
                                                data.icon = response.img;
                                            }
                                        },
                                        error: function(jqXHR, textStatus, errorThrown)
                                        {
                                            console.log('ERRORS: ' + textStatus);
                                        },
                                        complete: function () {
                                            console.log('complete');
                                            $.ajax({
                                                url: route+path+'/create-folder',
                                                type: 'POST',
                                                data: data,
                                                dataType: 'json',
                                                success: function (response, textStatus, jqXHR) 
                                                {

                                                },
                                                error: function(jqXHR, textStatus, errorThrown)
                                                {
                                                    console.log('ERRORS: ' + textStatus);
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    function enableInteractions() {
        $('.side-nav .side-nav-head').on({
            click: function (e) {
                e.stopPropagation();
                $(this).closest('.side-nav').toggleClass('collapse');
            }
        });
        $('input[name=maturity]').datepicker({
            format: 'dd. mm. yyyy',
            language: 'en',
            startDate: (new Date()),
            keepEmptyValues: true,
            autoclose: true,
        });
        $('.input-trigger').on('click', function(e) {
            e.stopPropagation();
            var trigger = $(this).attr('data-trigger');
            if(typeof trigger !== 'undefined') {
                $('#'+trigger).focus();
            }
        });
        $('.icon-preview').on({
            dragenter: function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).css('border', '1px dashed #b3b3b3');
            },
            dragover: function (e) {
                e.stopPropagation();
                e.preventDefault();
            },
            dragleave: function(e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).css('border', '1px solid #dcdcdc');
            },
            drop: function (e) {
                e.preventDefault();
                var file = e.originalEvent.dataTransfer.files[0];
                $(this).css('border', '1px solid #dcdcdc');
                handleFileUpload(this, file);
            }
        });
        // prevent document file drop
        $(document).on({
            dragenter: function (e) {
                e.stopPropagation();
                e.preventDefault();
            },
            dragover: function (e) {
                e.stopPropagation();
                e.preventDefault();
            },
            drop: function (e) {
                e.stopPropagation();
                e.preventDefault();
            }
        });


        $('input[name=android_icon], input[name=ios_icon]').on('change', function(e) {
            e.stopPropagation();
            console.log(e.target.files);
            var preview = $('.icon-preview[data-trigger='+$(this).attr('id')+']');
            handleFileUpload(preview, this.files[0]);
        });

        //input interactions
        $('input[required]').each(function () {
            $(this).on('blur', function (e) {
                validateInput($(this));
            });
        });
        $('#identifier').on({
            blur: function () {
                $(this).val($(this).val().toLowerCase());
                if($('#scheme').val() === '') {
                    $('#scheme').val($(this).val());
                }
                var identifier = $('#identifier').val().split('.');
                $('#project-name').val(identifier[identifier.length-1]);
                validateInput($('#project-name'));
            },
            focus: function () {
                if($(this).val() === '') {
                    $(this).val('net.plazz.mea.');
                }
            }
        });
        $('#app-name').bind('blur', function () {
            if($('#app-name-android').val() === '') {
                $('#app-name-android').val($(this).val());
            }
             if($('#app-name-ios').val() === '') {
                $('#app-name-ios').val($(this).val());
            }
        });
        initMenuActions();
    }



    $(document).ready(function() {
        $('input[type=text][required]').each(function () {
            var id = $(this).attr('id');
            var lbl = $('label[for="'+id+'"]');
            lbl.text(lbl.text()+' *');
        });

        $.get(route+path+'/tl', function (response) {
            DB = response;
        });
        enableInteractions();
    });

})();