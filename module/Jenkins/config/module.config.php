<?php 

namespace Jenkins;

use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Navigation\Page\Mvc;
use Jenkins\Model\Storage\JenkinsFolderStorageInterface;
use Jenkins\Model\Storage\JenkinsFolderStorageFactory;
use Jenkins\Model\Storage\JenkinsJobStorageInterface;
use Jenkins\Model\Storage\JenkinsJobStorageFactory;
use Jenkins\Model\Storage\JenkinsOptionStorageInterface;
use Jenkins\Model\Storage\JenkinsOptionStorageFactory;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [

    'router' => [
        'routes' => [
            'index' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
            'sync' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/sync',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'sync',
                    ],
                ],
            ],
            'new' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/new',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'create',
                    ],
                    'may_terminate' => true,
                    'child_routes' => [
                        'modify' => [

                        ],
                    ],
                ],
            ],
             'tl' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/new/:action',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'tl|get-folder|save-icon|create-folder',
                    ],
                    'may_terminate' => true,
                ],
            ],
        ],
    ],

    'controllers' => [
         'factories' => [
                Controller\IndexController::class => Controller\IndexControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            JenkinsFolderStorageInterface::class => JenkinsFolderStorageFactory::class,
            JenkinsJobStorageInterface::class => JenkinsJobStorageFactory::class,
            JenkinsOptionStorageInterface::class => JenkinsOptionStorageFactory::class,
        ],
    ],
    
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_path_stack' => [
                __DIR__ . '/../view',
        ],
    ],
];

?>