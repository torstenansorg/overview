<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsOption;
use Jenkins\Model\Hydrator\JenkinsOptionHydrator;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Factory\FactoryInterface;


class JenkinsOptionStorageFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    )
    {
        $dbAdapter = $container->get(Adapter::class);
        $resultSetPrototype = new HydratingResultSet(
            new JenkinsOptionHydrator(),
            new JenkinsOption()
        );

        $tableGateway = new Tablegateway(
            'options', $dbAdapter, null, $resultSetPrototype
        );

        return (new JenkinsOptionStorage($tableGateway));
    }
}