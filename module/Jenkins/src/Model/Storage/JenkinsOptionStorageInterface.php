<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsOption;

interface JenkinsOptionStorageInterface
{
    
    public function getOption ($key);
    public function setOption (JenkinsOption $jo);
    public function updateOption (JenkinsOption $jo);
    
}