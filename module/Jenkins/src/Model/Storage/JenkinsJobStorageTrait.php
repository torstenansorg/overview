<?php

namespace Jenkins\Model\Storage;

trait JenkinsJobStorageTrait {

    /**
    * @var JenkinsFolderStorageInterface $folderStorage the storage
    */    
    public $jobStorage;

    public function setJobStorage(JenkinsJobStorageInterface $storage) {
        $this->jobStorage = $storage;
    }

    public function clearJobStorage() {

    }

    public function getJobStorage() {
        return $this->jobStorage;
    }
}