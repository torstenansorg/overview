<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsJob;

interface JenkinsJobStorageInterface
{
    public function truncate();
    public function getJobsFromFolder($folder_id = null, $type = null);
    public function getJobByUrl($url = null);
    public function insertJob(JenkinsJob $jj);
    public function updatejob(JenkinsJob $jj);
    public function deleteJob(JenkinsJob $jj);
    public function getIdentifier();
    
}