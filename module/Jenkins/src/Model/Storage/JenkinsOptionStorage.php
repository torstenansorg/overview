<?php

namespace Jenkins\Model\Storage;

use Zend\Db\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\TableGatewayInterface;
use Jenkins\Model\Storage\JenkinsOptionStorageInterface;
use Jenkins\Model\Entity\JenkinsOption;

class JenkinsOptionStorage implements JenkinsOptionStorageInterface
{
    
    private $tableGateway;
    private $hydrator;

    public function __construct(TableGatewayInterface $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
        $resultSetPrototyp = $this->tableGateway->getResultSetPrototype();
        $this->hydrator = $resultSetPrototyp->getHydrator();
    }

    public function getOption ($key) {
        $select = $this->tableGateway->getSql()->select();
        $select->where(['options.option_key' => $key]);
        return ($this->tableGateway->selectWith($select)->current());
    }

    public function setOption (JenkinsOption $jo)
    {   
        $data = $this->hydrator->extract($jo);
        $insert = $this->tableGateway->getSql()->insert();
        $insert->values($data);
        return ($this->tableGateway->insertWith($insert) > 0);
    }

    public function updateOption(JenkinsOption $jo) {
        $data = $this->hydrator->extract($jo);
        $update = $this->tableGateway->getSql()->update();
        $update->set($data);
        $update->where(['options.option_key' => $jo->getOptionKey()]);
        return $this->tableGateway->updateWith($update);
    }

}

?>