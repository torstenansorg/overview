<?php

namespace Jenkins\Model\Storage;

use Zend\Db\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\TableGatewayInterface;
use Jenkins\Model\Storage\JenkinsFolderStorageInterface;
use Jenkins\Model\Entity\JenkinsFolder;

class JenkinsFolderStorage implements JenkinsFolderStorageInterface
{
    
    private $tableGateway;
    private $hydrator;

    public function __construct(TableGatewayInterface $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
        $resultSetPrototyp = $this->tableGateway->getResultSetPrototype();
        $this->hydrator = $resultSetPrototyp->getHydrator();
    }

    public function getFolders($order = null) {
        $select = $this->tableGateway->getSql()->select();
        if($order == 'maturity') {
            $select->order(['maturity']);
        } else {
            $select->order(['status' => 'ASC']);
        }
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getFolderByUrl($url = null) 
    {
        if(is_null($url)) {
            return false;
        }
        $select = $this->tableGateway->getSql()->select();
        $select->where(['folder.url' => $url]);
        $select->limit(1);
        return $this->tableGateway->selectWith($select)->current();
    }

    public function getFolderNames()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['name']);
        return $this->tableGateway->selectWith($select);
    }
    
    public function insertFolder(JenkinsFolder $jf)
    {   
        $data = $this->hydrator->extract($jf);
        $insert = $this->tableGateway->getSql()->insert();
        $insert->values($data);
        return ($this->tableGateway->insertWith($insert) > 0);

    }


    public function updateFolder(JenkinsFolder $jf)
    {   
        $data = $this->hydrator->extract($jf);
        $update = $this->tableGateway->getSql()->update();
        $update->set($data);
        $update->where(['folder.id' => $jf->getId()]);
        return $this->tableGateway->updateWith($update);
    }

    public function deleteFolder(JenkinsFolder $jf)
    {
        $delete = $this->tableGateway->getSql()->delete();
        $delete->where(['folder.id' => $jf->getId()]);
        return $this->tableGateway->deleteWith($delete) > 0;
    }
    
    

}

?>