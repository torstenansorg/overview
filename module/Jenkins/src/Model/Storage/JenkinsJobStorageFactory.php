<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsJob;
use Jenkins\Model\Hydrator\JenkinsJobHydrator;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Factory\FactoryInterface;


class JenkinsJobStorageFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    )
    {
        $dbAdapter = $container->get(Adapter::class);
        $resultSetPrototype = new HydratingResultSet(
            new JenkinsJobHydrator(),
            new JenkinsJob()
        );

        $tableGateway = new Tablegateway(
            'jobs', $dbAdapter, null, $resultSetPrototype
        );

        return (new JenkinsJobStorage($tableGateway));
    }
}