<?php

namespace Jenkins\Model\Storage;

trait JenkinsOptionStorageTrait {

    /**
    * @var JenkinsOptionStorageInterface $optionStorage the storage
    */    
    public $optionStorage;

    public function setOptionStorage(JenkinsOptionStorageInterface $storage) {
        $this->optionStorage = $storage;
    }

    public function clearOptionStorage() {

    }

    public function getOptionStorage() {
        return $this->optionStorage;
    }
}