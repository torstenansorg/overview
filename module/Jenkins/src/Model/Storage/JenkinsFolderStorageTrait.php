<?php

namespace Jenkins\Model\Storage;

trait JenkinsFolderStorageTrait {

    /**
    * @var JenkinsFolderStorageInterface $folderStorage the storage
    */    
    public $folderStorage;

    public function setFolderStorage(JenkinsFolderStorageInterface $storage) {
        $this->folderStorage = $storage;
    }

    public function clearFolderStorage() {

    }

    public function getFolderStorage() {
        return $this->folderStorage;
    }
}