<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsFolder;
use Jenkins\Model\Hydrator\JenkinsFolderHydrator;

use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\Factory\FactoryInterface;


class JenkinsFolderStorageFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    )
    {
        $dbAdapter = $container->get(Adapter::class);
        $resultSetPrototype = new HydratingResultSet(
            new JenkinsFolderHydrator(),
            new JenkinsFolder()
        );

        $tableGateway = new Tablegateway(
            'folder', $dbAdapter, null, $resultSetPrototype
        );

        return (new JenkinsFolderStorage($tableGateway));
    }
}