<?php

namespace Jenkins\Model\Storage;

use Zend\Db\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\TableGatewayInterface;
use Jenkins\Model\Storage\JenkinsJobStorageInterface;
use Jenkins\Model\Entity\JenkinsJob;

class JenkinsJobStorage implements JenkinsJobStorageInterface
{
    
    private $tableGateway;
    private $hydrator;

    public function __construct(TableGatewayInterface $tableGateway) 
    {
        $this->tableGateway = $tableGateway;
        $resultSetPrototyp = $this->tableGateway->getResultSetPrototype();
        $this->hydrator = $resultSetPrototyp->getHydrator();
    }

    public function truncate() {
        $this->tableGateway->delete(true);
    }

    public function getJobByUrl($url = null)
    {
        if(is_null($url)) {
            return false;
        }
        $select = $this->tableGateway->getSql()->select();
        $select->where(['jobs.url' => $url]);
        $select->limit(1);
        return $this->tableGateway->selectWith($select)->current();
    }

    public function getJobsFromFolder($folder_id = null, $type = null)
    {   
        if(is_null($folder_id)) {
            return false;
        }
        $select = $this->tableGateway->getSql()->select();
        if(is_null($type)) {
            $select->where(['jobs.folder_id' => $folder_id]);
        } else {
            $select->where(['jobs.folder_id' => $folder_id, 'jobs.type' => $type]);
        }
        return $this->tableGateway->selectWith($select)->toArray();
        
    }

    public function insertJob(JenkinsJob $jj)
    {   
        $data = $this->hydrator->extract($jj);
        $insert = $this->tableGateway->getSql()->insert();
        $insert->values($data);
        return ($this->tableGateway->insertWith($insert) > 0);
    }

    public function updatejob(JenkinsJob $jj)
    {}

    public function deleteJob(JenkinsJob $jj)
    {}

    public function getIdentifier()
    {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(['identifier']);
        return $this->tableGateway->selectWith($select);
    }

}

?>