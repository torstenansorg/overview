<?php

namespace Jenkins\Model\Storage;

use Jenkins\Model\Entity\JenkinsFolder;

interface JenkinsFolderStorageInterface
{
    
    public function getFolders($order = null);
    public function getFolderByUrl($url = null);
    public function getFolderNames();
    public function insertFolder(JenkinsFolder $jf);
    public function updateFolder(JenkinsFolder $jf);
    public function deleteFolder(JenkinsFolder $jf);
    
}