<?php

namespace Jenkins\Model\Entity;

class JenkinsJob 
{   

    private $id;
    private $name;
    private $type;
    private $url;
    private $version;
    private $build;
    private $folder;
    private $lastSuccess;
    private $description;
    private $status;
    private $cmsDomain;
    private $branch;
    private $identifier;

    public function setId($id) {
        $this->id = (integer) $id;
    }
    public function setName($name) {
        $this->name = trim($name);
    }
    public function setUrl($url) {
        $this->url = trim($url);
    }
    public function setDescription($description) {
        $this->description = trim($description);
    }
    public function setVersion($version) {
        $this->version = trim($version);
    }
    public function setBuild($build) {
        $this->build = trim($build);
    }
    public function setFolderId($fid) {
        $this->folder = trim($fid);
    }
    public function setLastSuccess($lastSuccess) {
        $this->lastSuccess = $lastSuccess;
    }
    public function setType($type){
        if(!in_array(strtolower($type), ['cms', 'web', 'android', 'ios', 'undefined'])) {
            $type = null;
        }
        $this->type = $type;
    }
    public function setStatus($status) {
        if(!in_array(strtolower($status), ['new', 'blocked', 'approved', 'uptodate'])) {
            $status = 'new';
        }
        $this->status = strtolower($status);
    }
    public function setCmsDomain($cms) {
        if($cms !== null) {
            $cms = str_replace(['http://', 'https://', 'www.', '.plazz.net'], '', $cms);
        }
        $this->cmsSubDomain = $cms;
    }
    public function setBranch($branch) {
        $this->branch = $branch;
    }
    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getType() {
        return $this->type;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getVersion() {
        return $this->version;
    }
    public function getBuild() {
        return $this->build;
    }
    public function getFolderId() {
        return $this->folder;
    }
    public function getLastSuccess() {
        return $this->lastSuccess;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getStatus() {
        return $this->status;
    }
    public function getCmsDomain() {
        return $this->cmsDomain;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

}