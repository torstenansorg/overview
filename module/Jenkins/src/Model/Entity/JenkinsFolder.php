<?php

namespace Jenkins\Model\Entity;

class JenkinsFolder 
{
    private $id;
    private $name;
    private $url;
    private $description;
    private $icon;
    private $status;
    private $maturity;

    public function setId($id) {
        $this->id = (integer) $id;
    }
    public function setName($name) {
        $this->name = trim($name);
    }
    public function setUrl($url) {
        $this->url = trim($url);
    }
    public function setDescription($description) {
        $this->description = trim($description);
    }
    public function setIcon($icon) {
        $this->icon = trim($icon);
    }
    public function setStatus($status) {
        if(!in_array(strtolower($status), ['new', 'blocked', 'approved', 'uptodate'])) {
            $status = 'new';
        }
        $this->status = strtolower($status);
    }
    public function setMaturity($maturity)
    {
        $this->maturity = $maturity;
    }

    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getUrl() {
        return $this->url;
    }
    public function getDescription() {
        return $this->description;
    }
    public function getIcon() {
        return $this->icon;
    }
    public function getStatus() {
        return $this->status;
    }
    public function getMaturity() 
    {
        return $this->maturity;
    }

    public function block() {
        $this->status = 'blocked';
    }

    public function approve() {
        $this->status = 'approved';
    }

}