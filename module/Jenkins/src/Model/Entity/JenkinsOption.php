<?php

namespace Jenkins\Model\Entity;

class JenkinsOption
{
    private $option_value;
    private $option_key;

    public function getOptionValue()
    {
        return $this->option_value;
    }

    public function getOptionKey() {
        return $this->option_key;
    }

    public function setOptionValue($value) 
    {
        $this->option_value = $value;
    }

    public function setOptionKey($key) {
        $this->option_key = $key;
    }

    
    
}