<?php

namespace Jenkins\Model\Repositories;

use Jenkins\Model\Entity\JenkinsFolder;
use Jenkins\Model\Entity\JenkinsJob;
use Jenkins\Model\Entity\JenkinsOption;

use Jenkins\Model\Hydrator\JenkinsFolderHydrator;
use Jenkins\Model\Hydrator\JenkinsJobHydrator;
use Jenkins\Model\Hydrator\JenkinsOptionHydrator;

use Jenkins\Model\Storage\JenkinsFolderStorageTrait;
use Jenkins\Model\Storage\JenkinsJobStorageTrait;
use Jenkins\Model\Storage\JenkinsOptionStorageTrait;

class Jenkins implements JenkinsInterface
{
    use JenkinsFolderStorageTrait;
    use JenkinsJobStorageTrait;
    use JenkinsOptionStorageTrait;

    private $user = '';
    private $pass = '';

    private $versions;
    private $url = 'http://buildserver.plazz:8080/job/Customers/api/';
    private $type = 'json';
    private $treeOptions = [
        'tree' => 'jobs[name,displayName,url,description,jobs[name,description,url,buildable,inQueue,lastCompletedBuild[displayName,timestamp]]]',
        'depth' => 2,
        'pretty' => TRUE
    ];
    
    private $curlOptions = [];

    public function getVersions() {
        return $this->versions;
    }    

    public function setCurlOptions(array $options) {
        if(!is_null($options)) {
            $this->curlOptions = $options;
        }
    }
    public function setJenkinsPass(string $pass) {
        if(!is_null($pass)) {
            $this->pass = $pass;
        }
    }
    public function setJenkinsUser(string $user) {
        if(!is_null($user)) {
            $this->user = $user;
        }
    }
    public function setType(string $type) {
        if(!is_null($type)) {
            $this->type = $type;
        } else {
            $this->type ='json';
        }
    }
    public function setTreeOptions(array $treeOptions) {
        if(!is_null($treeOptions)) {
            $this->treeOptions = $treeOptions;
        } else {
            $this->treeOptions = [];
        }
    }

    public function getCurlOptions() {
        return $this->curlOptions;
    }
    public function getJenkinsUser() {
        return $this->user;
    }
    public function getType() {
        return $this->type;
    }
    public function getTreeOptions() {
        return $this->treeOptions;
    }
    public function getJenkinsUri() {
        $j = null;
        if($this->url !== '') {
            $j = $this->url;
            if($this->type !== '') {
                if(substr($j, -1) !== '/') {
                    $j .= '/';
                }
                $j .= $this->type;
                if(count($this->treeOptions) > 0 ) {
                    $j .= '?';
                    $s = [];
                    foreach ($this->treeOptions as $key => $value) {
                        $s[] = $key.'='.(is_bool($value) ? ($value ? 'true' : false) : $value);
                    }
                    $j .= implode('&', $s);
                }
            }
        }
        return $j;
    }

    public function __construct()
    {   
        $cookies = '/tmp/cookies.txt';
        $this->setCurlOptions(
            [
                CURLOPT_FOLLOWLOCATION => TRUE,
                CURLOPT_HTTPHEADER => [
                    'Authorization: Basic ' . base64_encode($this->user . ':' . $this->pass),
                ],
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => 40,
                CURLOPT_COOKIESESSION => TRUE,
                CURLOPT_COOKIEJAR => '$cookie',
                CURLOPT_COOKIEFILE, '$cookie',
                CURLOPT_COOKIE => session_name() . '=' . session_id(),
                CURLOPT_URL => $this->getJenkinsUri()
            ]
        );
    }

    public function init() {
        $this->versions['android'] = $this->optionStorage->getOption('version_android')->getOptionValue();
        $this->versions['cms'] = $this->optionStorage->getOption('version_cms')->getOptionValue();
        $this->versions['ios'] = $this->optionStorage->getOption('version_ios')->getOptionValue();
        $this->versions['web'] = $this->optionStorage->getOption('version_web')->getOptionValue();
    }

    public function fetchJobs() 
    {   
        $ch = curl_init();
        curl_setopt_array($ch, $this->curlOptions);
        $result = json_decode(curl_exec($ch), true);
        if(!isset($result['jobs'])) {
            return null;
        }

        $this->jobStorage->truncate();

        foreach ($result['jobs'] as $folder) {
            $jobs = $folder['jobs'];
            unset($folder['jobs']);
            unset($folder['_class']);

            $jenkinsFolder = new JenkinsFolder();
            $jenkinsFolderHydrator = new JenkinsFolderHydrator();
            $jenkinsFolderHydrator->hydrate($folder, $jenkinsFolder);

            $f = $this->folderStorage->getFolderByUrl($folder['url']);

            if(!$f) {
                $this->folderStorage->insertFolder($jenkinsFolder);
                $f = $this->folderStorage->getFolderByUrl($folder['url']);
            } else {
                $f->setDescription($folder['description']);
                $f->setUrl($folder['url']);
                $f->setName($folder['name']);
                $this->folderStorage->updateFolder($f);
            }

            $k = ['cms' => false, 'web' => false, 'ios' => false, 'android' => false];

            foreach($jobs as $job) {
                switch (true) {
                    case preg_match("/^cms/", strtolower($job['name'])):
                        $type = 'cms';
                        break;
                    case preg_match("/^web/", strtolower($job['name'])):
                        $type = 'web';
                        break;
                    case preg_match("/^android/", strtolower($job['name'])):
                        $type = 'android';
                        break;
                    case preg_match("/^ios/", strtolower($job['name'])):
                        $type = 'ios';
                        break;
                    default:
                        $type = 'undefined';
                        break;
                }
                $jenkinsJob = new JenkinsJob();
                $jenkinsJobHydrator = new JenkinsJobHydrator();
                $jenkinsJobHydrator->hydrate($job, $jenkinsJob);

                if(isset($job['lastCompletedBuild'])) {
                    $jenkinsJob->setLastSuccess($job['lastCompletedBuild']['timestamp']);
                    $v = explode(" - v", $job['lastCompletedBuild']['displayName']);
                    if(count($v) > 1) {
                        $jenkinsJob->setBuild(substr($v[0],1));
                        $jenkinsJob->setVersion($v[1]);
                        if($type !== 'undefined' && version_compare($v[1], $this->versions[$type]) >= 0) {
                            $jenkinsJob->setStatus('uptodate');
                            if(array_key_exists($type, $k)) {
                                $k[$type] = true;
                            }
                        } else {
                            $jenkinsJob->setStatus('approved');
                        }
                    }
                    else {
                        $jenkinsJob->setBuild($job['lastCompletedBuild']['displayName']);
                        $jenkinsJob->setVersion('2.0.0');
                        $jenkinsJob->setStatus('approved');
                    }
                } else {
                    $jenkinsJob->setStatus('new');
                }

                $jenkinsJob->setType($type);
                $jenkinsJob->setFolderId($f->getId());

                $j = $this->jobStorage->getJobByUrl($jenkinsJob->getUrl());
                if(!$j) {
                    $this->jobStorage->insertJob($jenkinsJob);
                } else {
                    $this->jobStorage->updateJob($jenkinsJob);
                }          
            }
            $status = $f->getStatus();

            if($status !== 'blocked') {
                if($k['cms'] && $k['web'] && $k['ios'] && $k['android']) {
                    $status = 'uptodate';
                } else {
                    $status = 'approved';
                }
                $f->setStatus($status);
                $this->folderStorage->updateFolder($f);
            }
        }
        return $result;
    }

    public function getAllFolder($order = null)
    {
        return $this->folderStorage->getFolders($order);
    }

    public function getFolderNames()
    {
        return $this->folderStorage->getFolderNames();
    }

    public function getJobsFromFolder($folder_id)
    {
        $j = [];
        $j['cms']      = $this->jobStorage->getJobsFromFolder($folder_id, 'cms');
        $j['web']      = $this->jobStorage->getJobsFromFolder($folder_id, 'web');
        $j['android']  = $this->jobStorage->getJobsFromFolder($folder_id, 'android');
        $j['ios']      = $this->jobStorage->getJobsFromFolder($folder_id, 'ios');
        return $j;
    }

    public function getFolderFromData(array $data = [])
    {   
        $folder = new JenkinsFolder();
        $folderHydrator = new JenkinsFolderHydrator();
        $folderHydrator->hydrate($data, $folder);
        return $folder;
    }

    public function createFolder(JenkinsFolder $jf)
    {
        if(!$jf->getId()) {
            return $this->folderStorage->insertFolder($jf);
        } else {
            return $this->folderStorage->updateFolder($jf);
        }
    }

    public function getIdentifier() 
    {
        return $this->jobStorage->getIdentifier();
    }


    public function createJob($folder = '')
    {

    }

    public function deleteJob($url = '') 
    {

    }

    public function updateJob($url = '')
    {

    }



}