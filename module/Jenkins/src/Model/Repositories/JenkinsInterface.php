<?php

namespace Jenkins\Model\Repositories;

use Jenkins\Model\Entity\JenkinsFolder;

interface JenkinsInterface
{

    public function getAllFolder($order = null);
    public function fetchJobs();

    public function createFolder(JenkinsFolder $jf);

    public function createJob($folder = '');
        
    public function deleteJob($url = '');
    
    public function updateJob($url = '');

    public function getFolderNames();
    public function getIdentifier();
    
}