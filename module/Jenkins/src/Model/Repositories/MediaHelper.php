<?php

namespace Jenkins\Model\Repositories;

use Exception;

class MediaHelper implements MediaHelperInterface
{
    private $dir = './public/uploads';

    public function getDir()
    {
        return $this->dir;
    }

    public function setDir(string $dir)
    {
        $this->dir = $dir;
    }

    public function __construct()
    {
        if(!is_dir($this->dir)) {
            if(!mkdir($this->dir)) {
                throw new Exception("could not create folder for upload", error_get_last());
            }
        }
    }

    public function uploadFile(array $file)
    {
        $type = explode('/' , $file['type']);
        $name = basename(uniqid().'.'.$type[1]);
        if(move_uploaded_file($file['tmp_name'], "$this->dir/$name")) {
            return $name;
        }
        return false;
    }



    public function getFiles() 
    {

    }

    public function getFileByName()
    {

    }
    
    public function resizeImage()
    {

    }
}