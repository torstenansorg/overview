<?php

namespace Jenkins\Model\Repositories;

interface MediaHelperInterface 
{
    public function getFiles();
    public function getFileByName();
    
    public function uploadFile(array $file);
    public function resizeImage();
}