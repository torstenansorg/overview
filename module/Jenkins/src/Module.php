<?php

/**
 * @author Torsten Ansorg <ta@plazz.ag>
 */

namespace Jenkins;

use Zend\Config\Factory;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\InitProviderInterface;
use Zend\ModuleManager\ModuleManagerInterface;

class Module implements ConfigProviderInterface, InitProviderInterface
{
    public function init(ModuleManagerInterface $manager) 
    {   
        if(!defined('JENKINS_MODULE_ROOT')) {
            define('JENKINS_MODULE_ROOT', realpath(__DIR__ . '/..'));
        }
    }

    public function getConfig()
    {
        return Factory::fromFile(
            JENKINS_MODULE_ROOT . '/config/module.config.php'
        );
    }

}