<?php

namespace Jenkins\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Jenkins\Model\Repositories\Jenkins;
use Jenkins\Model\Repositories\MediaHelper;
use Jenkins\Model\Entity\JenkinsFolder;
use Jenkins\Model\Hydrator\JenkinsFolderHydrator;

class IndexController extends AbstractActionController
{
    private $jenkins;

    private function getBaseUrl() {
        $uri = $this->getRequest()->getUri();
        return sprintf('%s://%s', $uri->getScheme(), $uri->getHost());
    }

    public function setJenkins($jenkins) {
        $this->jenkins = $jenkins;
    }

    public function syncAction() {
        $this->jenkins->fetchJobs();
        $folder = $this->jenkins->getAllFolder()->toArray();
        for ($i=0; $i < count($folder); $i++) {
            $j = $this->jenkins->getJobsFromFolder($folder[$i]['id']);
            $folder[$i]['jobs'] = $j;
        }
        $vm = new ViewModel([
            'folder' => $folder,
            'versions' => $this->jenkins->getVersions()
        ]);
        $vm->setTemplate('/Jenkins/index/index');
        return $vm;
    }

    public function indexAction()
    {   
        $folder = $this->jenkins->getAllFolder()->toArray();
        for ($i=0; $i < count($folder); $i++) {
            $j = $this->jenkins->getJobsFromFolder($folder[$i]['id']);
            $folder[$i]['jobs'] = $j;
        }

        return new ViewModel([
            'folder' => $folder,
            'versions' => $this->jenkins->getVersions()
        ]);
    }

    public function createAction() 
    {
        
        return new ViewModel([
            'base' => $this->getBaseUrl(),
            'path' => '/new',
        ]);
    }

    public function createFolderAction () {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost()->toArray();
            $jenkinsFolder = new JenkinsFolder();
            $folderHydrator = new JenkinsFolderHydrator();
            $folderHydrator->hydrate($data, $jenkinsFolder);
            return new JsonModel(['status' => $this->jenkins->folderStorage->insertFolder($jenkinsFolder)]); 
        }
    }

    public function saveIconAction() {
        if ($this->getRequest()->isPost()) {
            $files = $this->getRequest()->getFiles()->toArray();
            if (isset($files['icon'])) {
                if ($files['icon']['type'] !== 'image/png') {
                    return new JsonModel(['status' => 'wrong file type']);
                }
                $url = (new MediaHelper())->uploadFile($files['icon']);
                return new JsonModel(['img' => $url, 'status' => 'success']);
            }
        } 
        return new JsonModel(['status' => 'no file']);
    }

    public function tlAction() {


        $folder = $this->jenkins->getFolderNames()->toArray();
        $folderNames = [];
        foreach ($folder as $f) {
            $folderNames[] = $f['name'];
        }

        $jobs = $this->jenkins->getIdentifier()->toArray();
        $identifier = [];
        foreach ($jobs as $j) {
            if(!is_null($j['identifier'])) {
                $identifier[] = $j['identifier'];
            }
        }

        return new JsonModel([
            'folder' => $folderNames,
            'identifier' => $identifier,
        ]);
    }

    public function getFolderAction() {
        $folder = $this->jenkins->getFolderNames()->toArray();
        $folderNames = [];
        foreach ($folder as $f) {
            $folderNames[] = $f['name'];
        }
        return new JsonModel(['folder' => $folderNames]);
    }

}