<?php

namespace Jenkins\Controller;

use Jenkins\Model\Repositories\Jenkins;
use Jenkins\Model\Storage\JenkinsFolderStorageInterface;
use Jenkins\Model\Storage\JenkinsJobStorageInterface;
use Jenkins\Model\Storage\JenkinsOptionStorageInterface;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    )
    {
        $controller = new IndexController();
        
        $folderStorage = $container->get(JenkinsFolderStorageInterface::class);
        $jobStorage = $container->get(JenkinsJobStorageInterface::class);
        $optionStorage = $container->get(JenkinsOptionStorageInterface::class);

        $jenkins = new Jenkins();
        $jenkins->setFolderStorage($folderStorage);
        $jenkins->setJobStorage($jobStorage);
        $jenkins->setOptionStorage($optionStorage);

        $jenkins->init();
        
        $controller->setJenkins($jenkins);

        return $controller;
    }
}