<?php

return [
    'db' => [
        'driver' => 'pdo',
        'dsn' => 'mysql:dbname=overview;host=localhost;charset=utf8',
        'user' => 'root',
        'pass' => 'root',
    ],
    'view_manager' => [
        'display_exceptions' => true,
    ],
];
